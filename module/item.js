/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Item}
 */
export class mItem extends Item {

    async prepareData() {
      super.prepareData();
  
      // Get the Item's data
      const itemData = this.data
  
      // Get the data of the actor that owns the item
      const actorData = this.actor ? this.actor.data : {}
  
      // Assign Characteristic Dropdowns for Skills
      if (this.type === 'standardSkill'||this.type === "professionalSkill"||this.type === "combatStyle"||this.type === "magicSkill"||this.type === "passion") {
        if (this.isOwned && actorData) {
          this._prepareSkills(actorData, itemData);
        }
      }

      //Assign Armor Lists for each Hit Location
      if (this.isOwned && actorData){
        if (this.type === 'hitLocation'){
          this._prepareHitLocations(actorData, itemData);
        }
      }

      //Prepare Equipment Item Data
      if (this.isOwned && actorData) {
        if (this.type === "equipment") {
          this._prepareEquipmentItems(actorData, itemData);
        }
      }
      
    }
  
      /**
     * Prepare data specific to skill items
     * @param {*} itemData
     * @param {*} actorData
     */
  
    _prepareSkills(actorData, itemData) {
      const data = itemData.data;

            //Calculate Characteristic 1
        if (data.char1 === "str") {
            data.base = actorData.data.characteristics.str.value;
        } else if (data.char1 === "con") {
            data.base = actorData.data.characteristics.con.value;
        } else if (data.char1 === "siz") {
            data.base = actorData.data.characteristics.siz.value;
        } else if (data.char1 === "dex") {
            data.base = actorData.data.characteristics.dex.value;
        } else if (data.char1 === "int") {
            data.base = actorData.data.characteristics.int.value;
        } else if (data.char1 === "pow") {
            data.base = actorData.data.characteristics.pow.value;
        } else if (data.char1 === "cha") {
            data.base = actorData.data.characteristics.cha.value;
        }

        //Calculate Characteristic 2 and Base
        if (data.char2 === "str") {
            data.base = data.base + actorData.data.characteristics.str.value;
        } else if (data.char2 === "con") {
            data.base = data.base + actorData.data.characteristics.con.value;
        } else if (data.char2 === "siz") {
            data.base = data.base + actorData.data.characteristics.siz.value;
        } else if (data.char2 === "dex") {
            data.base = data.base + actorData.data.characteristics.dex.value;
        } else if (data.char2 === "int") {
            data.base = data.base + actorData.data.characteristics.int.value;
        } else if (data.char2 === "pow") {
            data.base = data.base + actorData.data.characteristics.pow.value;
        } else if (data.char2 === "cha") {
            data.base = data.base + actorData.data.characteristics.cha.value;
        }

        //Calculate total Skill %
        data.value = data.base + data.xp;

    }
  
    /**
     * Prepare data specific to armor items
     * @param {*} itemData
     * @param {*} actorData
     */

    _prepareHitLocations(actorData, itemData){
      const data = itemData.data;

      const armor = actorData.items.find(i => i.data._id == data.equippedArmorId);
      const wornArmor = [];

      //Create Armor Selection Dropdown Options on Hit Locations
      data.armorList.push({name: "None"});
      for (let i of this.actor.items.filter(x => x.type === 'armor')){
        itemData.data.armorList.push(i);
      }

      if (armor !== undefined){
        data.equippedArmorName = `[${armor.data.data.armorValue}] - ${armor.name}`;
      }

      //Calculate Max HP per Hit Location for Character Types
      if (this.actor.type === "character"||this.actor.type === "creature") {
        if (data.autoCalcHP) {
          let sizConTotal = actorData.data.characteristics.siz.value + actorData.data.characteristics.con.value;
          let sizConArray = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75];

          if (this.name.includes("Leg")||this.name.includes("Head")||this.name.includes("Tail")||this.name.includes("End-Length")||this.name.includes("Fore-Length")||this.name.includes("Pincer")) {
            for (let group of sizConArray) {
              if (sizConTotal <= group && sizConTotal >= (group - 4)) {
                data.hpMax = Math.ceil(group/5) + data.hpBonus;
              }
            }
          }

          if (this.name.includes("Arm")||this.name.includes("Wing")||this.name.includes("Forelimb")||this.name.includes("Trunk")||this.name.includes("Fin")) {
            for (let group of sizConArray) {
              if (sizConTotal <= group && sizConTotal >= (group - 4)) {
                data.hpMax = (Math.ceil(group/5) -1) + data.hpBonus;
                //Set values back to 1 if they are 0
                if (data.hpMax < 1) {
                  data.hpMax = 1;
                }
              }
            }
          }

          if (this.name.includes("Abdomen")||this.name.includes("Hindquarters")||this.name.includes("Metathorax")||this.name.includes("Mid-Length")) {
            for (let group of sizConArray) {
              if (sizConTotal <= group && sizConTotal >= (group - 4)) {
                data.hpMax = (Math.ceil(group/5) +1) + data.hpBonus;
              }
            }
          }

          if (this.name.includes("Chest")||this.name.includes("Forequarters")||this.name.includes("Prothorax")||this.name.includes("Thorax")||this.name.includes("Cephalothorax")) {
            for (let group of sizConArray) {
              if (sizConTotal <= group && sizConTotal >= (group - 4)) {
                data.hpMax = (Math.ceil(group/5) +2) + data.hpBonus;
              }
            }
          }
        }
      }

      // Add Broken Heart Icon to Hovered Hit Location if Wounded
      if (data.wounded) {
        data.woundIcon = `<i class="fas fa-heart-broken"></i>`;
      } else {
        data.woundIcon = "";
      }

    }

    _prepareEquipmentItems(actorData, itemData) {
      // Add Feather Icon if not counted toward total ENC
      const data = itemData.data;

      if (data.encEffect) {
        data.weightlessIcon = `<i class="fas fa-feather-alt"></i>`;
      } else {
        data.weightlessIcon = "";
      }
    }
  
  }