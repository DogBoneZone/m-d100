/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the m-d100 system.
 * @extends {Actor}
 */
export class mActor extends Actor {

    async _preCreate(data, options, user){
        await super._preCreate(data, options, user);

        // Add Default Hit Locations and Standard Skills

        if (data.type === "character") {
            this.data.token.update({vision: true, actorLink: true, disposition: 1});
            
            let hitLocationsPack = game.packs.get("m-d100.human-hit-locations");
            let collection = await hitLocationsPack.getDocuments();
            
            collection.sort(function (a, b) {
              let rangeStart1 = a.data.data.rangeStart;
              let rangeStart2 = b.data.data.rangeStart;
              if (rangeStart1 < rangeStart2) {
                return -1;
              } if (rangeStart1 > rangeStart2) {
                return 1;
              }
              return 0
            });
            this.data.update({
              items: collection.map(i => i.toObject())
            });
        }

        if (data.type === "character"||data.type === "creature") {
            if (data.type === "creature") {
                this.data.token.update({vision: true, disposition: -1});
            }

            let skillPack = game.packs.get("m-d100.standard-skills");
            let collection2 = await skillPack.getDocuments();

            collection2.sort(function (a, b) {
                let nameA = a.name.toUpperCase();
                let nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                    return -1
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0
            });
            this.data.update({
                items: collection2.map(i => i.toObject())
            });
            
          }
      }

    prepareData() {
      super.prepareData();
  
      const actorData = this.data;
      const flags = actorData.flags;
  
      // Make separate methods for each Actor type (character, npc, etc.) to keep
      // things organized.
      if (actorData.type === 'character'||actorData.type === 'creature') this._prepareBaseData(actorData);
      if (actorData.type === 'creature') this._prepareCreatureData(actorData);
      
    }
  
    /**
     * Prepare Character type specific data
     */
    _prepareBaseData(actorData) {
        const data = actorData.data;

        //Derived Attributes
        const fixedActionPoints = game.settings.get("m-d100", "fixedActionPoints");

        //Set Static Action Points at 2 for ALL if system setting is checked
        if (fixedActionPoints) {
            data.attributes.actionPoints.max = 2;
        } else {
            data.attributes.actionPoints.max = Math.ceil((data.characteristics.int.value + data.characteristics.dex.value) / 12);
        }


        data.attributes.damageMod.value = this._calculateDamageMod(actorData);
        data.attributes.xpMod.value = Math.ceil(data.characteristics.cha.value / 6) - 2;
        data.attributes.healRate.value = Math.ceil(data.characteristics.con.value /6);
        data.attributes.initBonus.value = Math.ceil((data.characteristics.int.value + data.characteristics.dex.value) / 2);
        data.attributes.luckPoints.max = Math.ceil(data.characteristics.pow.value / 6);
        data.attributes.magicPoints.max = data.characteristics.pow.value;
        data.attributes.tenacity.max = data.characteristics.pow.value;

        //Armor Penalty
        data.encStats.armorPenalty = Number(Math.ceil(this._calcArmorPenalty(actorData)/5).toFixed(0));

        //Derived movement speeds
        data.attributes.movement.walk = data.attributes.movement.base;
        data.attributes.movement.run = (((data.attributes.movement.walk + (this._grabAthleticsValue(actorData)/25)*0.5)*3) - data.encStats.armorPenalty).toFixed(0);
        data.attributes.movement.sprint = (((data.attributes.movement.walk + (this._grabAthleticsValue(actorData)/25))*5) - data.encStats.armorPenalty).toFixed(0);
        data.attributes.movement.jumpH = (((data.height/100)*2 + (this._grabAthleticsValue(actorData)/20)) - Math.ceil(data.encStats.armorPenalty/2)).toFixed(0);
        data.attributes.movement.jumpV = (((data.height/100) + ((this._grabAthleticsValue(actorData)/20)*0.2)) - Math.ceil(data.encStats.armorPenalty/2)).toFixed(0);
        data.attributes.movement.climbRough = data.attributes.movement.walk - Math.ceil(data.encStats.armorPenalty/2);
        data.attributes.movement.climbSteep = data.attributes.movement.walk - data.encStats.armorPenalty;
        data.attributes.movement.climbSheer = data.attributes.movement.walk - (data.encStats.armorPenalty * 2);
        data.attributes.movement.swim = (data.attributes.movement.walk + (this._grabSwimValue(actorData)/20)).toFixed(0);

        const swimENCValue = ((data.attributes.movement.swim/2) - data.encStats.armorPenalty).toFixed(0);

        if (swimENCValue > 0) {
            data.attributes.movement.canSwim = "Can Swim Normally";
        } else if (swimENCValue == 0) {
            data.attributes.movement.canSwim = "Can Float But Cannot Move";
        } else if (swimENCValue < 0) {
            data.attributes.movement.canSwim = "Cannot Swim, Will Sink";
        }

        //Calculate ENC Values
        data.encStats.burdenedENC = data.characteristics.str.value * 2;
        data.encStats.overloadedENC = data.characteristics.str.value * 3;
        data.encStats.armorENC = this._calculateArmorEnc(actorData);
        data.encStats.encTotal = (this._calculateTotalENC(actorData) + data.encStats.armorENC).toFixed(1);

        // //Calculate Spell Count for Magic Spell List : This is not needed currently, but could be useful to show how many spells of a certain type a character has
        // data.count.animism = actorData.items.filter(i => i.type === 'spell' && i.data.data.discipline === 'animism').length;
        // data.count.folkMagic = actorData.items.filter(i => i.type === 'spell' && i.data.data.discipline === 'folkMagic').length;
        // data.count.sorcery = actorData.items.filter(i => i.type === 'spell' && i.data.data.discipline === 'sorcery').length;
        // data.count.mysticism = actorData.items.filter(i => i.type === 'spell' && i.data.data.discipline === 'mysticism').length;
        // data.count.theism = actorData.items.filter(i => i.type === 'spell' && i.data.data.discipline === 'theism').length;

        //Calculate Fatigue Effects
        if (data.fatigue.current === "tired") {
            data.attributes.movement.walk = data.attributes.movement.walk - 1;
        } else if (data.fatigue.current === "wearied") {
            data.attributes.initBonus.value = data.attributes.initBonus.value - 2;
            data.attributes.movement.walk = data.attributes.movement.walk - 2;
        } else if (data.fatigue.current === "exhausted") {
            data.attributes.movement.walk = data.attributes.movement.walk/2;
            data.attributes.initBonus.value = data.attributes.initBonus.value - 4;
            data.attributes.actionPoints.max = data.attributes.actionPoints.max - 1;
        } else if (data.fatigue.current === "debilitated") {
            data.attributes.movement.walk = data.attributes.movement.walk/2;
            data.attributes.initBonus.value = data.attributes.initBonus.value - 6;
            data.attributes.actionPoints.max = data.attributes.actionPoints.max - 2;
        } else if (data.fatigue.current === "incapacitated") {
            data.attributes.movement.walk = 0;
            data.attributes.initBonus.value = data.attributes.initBonus.value - 8;
            data.attributes.actionPoints.max = data.attributes.actionPoints.max - 3;
        } else if (data.fatigue.current === "semiConscious"||data.fatigue.current === "comatose"||data.fatigue.current === "dead") {
            data.attributes.movement.walk = 0;
            data.attributes.initBonus.value = 0;
            data.attributes.actionPoints.max = 0;
        }

        //Calculate # of Wounds and Locations to show on Combat Tab (This needs to be calculated AFTER the calculation for Wounds to automatically check the wound boolean on items)
        let woundLocations = actorData.items.filter(i => i.type === "hitLocation" && i.data.data.wounded);
        data.wounds = woundLocations.length;


    }

    _prepareCreatureData(actorData) {
        const data = actorData.data;

    }

    /*
        Start of Functions for use in Data Prep
    */

    _calculateDamageMod(actorData) {
        let mod = "";
        let siz = actorData.data.characteristics.siz.value;
        let str = actorData.data.characteristics.str.value;
        let total = Math.ceil((siz + str) / 5);

        if (total <= 1) {
            mod = "-1d8";
        } else if (total == 2) {
            mod = "-1d6";
        } else if (total == 3) {
            mod = "-1d4";
        } else if (total == 4) {
            mod = "-1d2";
        } else if (total == 5) {
            mod = "0";
        } else if (total == 6) {
            mod = "1d2";
        } else if (total == 7) {
            mod = "1d4";
        } else if (total == 8) {
            mod = "1d6";
        } else if (total == 9) {
            mod = "1d8";
        } else if (total == 10||total == 11) {
            mod = "1d10";
        } else if (total == 12||total == 13) {
            mod = "1d12"
        } else if (total == 14||total == 15) {
            mod = "2d6";
        } else if (total == 16||total == 17) {
            mod = "1d8+1d6";
        } else if (total == 18||total == 19) {
            mod = "2d8";
        } else if (total == 20||total == 21) {
            mod = "1d10+1d8";
        } else if (total == 22||total == 23) {
            mod = "2d10";
        } else if (total == 24||total == 25) {
            mod = "2d10+1d2";
        } else if (total == 26||total == 27) {
            mod = "2d10+1d4";
        } else if (total == 28||total == 29) {
            mod = "2d10+1d6";
        } else if (total == 30||total == 31) {
            mod = "2d10+1d8";
        }
        return mod
    }

    _calcArmorPenalty(actorData) {
        let hitLocations = actorData.items.filter(item => item.type === "hitLocation");
        let equippedArmors = [];

        for (let i of hitLocations){
            if (i.data.data.equippedArmorId !== null && i.data.data.equippedArmorId !== ''){
                equippedArmors.push(i.data.data.equippedArmorId);
            }
        }

        let total = 0;
        for (let id of equippedArmors) {
            let item = actorData.items.find(i => i.id == id);
            if (item !== undefined) {
                total = Number(total + (item.data.data.enc * item.data.data.qty));
            }
        }
        return total
    }

    _grabAthleticsValue(actorData) {
        let athletics = actorData.items.find(item => item.type === "standardSkill" && item.name === "Athletics");
        let value = 0;
        if (athletics !== undefined && athletics !== null) {
            value = athletics.data.data.value;
        }

        return value
    }

    _grabSwimValue(actorData) {
        let swim = actorData.items.find(item => item.type === "standardSkill" && item.name === "Swim");
        let value = 0;

        if (swim !== undefined && swim !== null) {
            value = swim.data.data.value;
        }

        return value
    }

    _calculateArmorEnc(actorData){
        let allArmors = actorData.items.filter(item => item.type === "armor");
        let hitLocations = actorData.items.filter(item => item.type === "hitLocation");
        let equippedArmors = [];

        // Push equipped armors into array
        for (let i of hitLocations){
            if (i.data.data.equippedArmorId !== null && i.data.data.equippedArmorId !== ''){
                equippedArmors.push(i.data.data.equippedArmorId);
            }
        }

        // Calculate ENC of worn armor
        let wornArmorENC = 0;
        for (let id of equippedArmors) {
            let item = actorData.items.find(i => i.id == id);
            if (item !== undefined) {
                wornArmorENC = wornArmorENC + (item.data.data.enc * item.data.data.qty)/2;
            }
        }

        // Calculate Total Armor ENC and subtract worn armor ENC
        let totalArmorENC = 0;
        for (let i of allArmors){
            totalArmorENC = Number(totalArmorENC + (i.data.data.enc * i.data.data.qty))
        }

        let total = 0;
        total = Number(totalArmorENC - wornArmorENC);
        return total
    }

    _calculateTotalENC(actorData){
        let allItems = actorData.items.filter(i => i.type !== "armor" && i.data.data.hasOwnProperty("enc"));
        
        let total = 0;
        for (let item of allItems){
            if (item.type === "equipment" && item.data.data.encEffect) {
                total = total;
            } else {
                total = total + (item.data.data.qty * item.data.data.enc);
            }
        }
        return total
    }

}