// Import Modules
import { mActor } from "./actor.js";
import { mItem } from "./item.js";
import { mItemSheet } from "./item-sheet.js";
import { mActorSheet } from "./actor-sheet.js";


/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
    console.log(`Initializing m-d100 System`);
  
      /**
       * Set an initiative formula for the system
       * @type {String}
       */
      CONFIG.Combat.initiative = {
      formula: "1d10 + @attributes.initBonus.value",
      decimals: 0
    };
  
      // Define custom Entity classes
    CONFIG.Actor.documentClass = mActor;
    CONFIG.Item.documentClass = mItem;
  
    // Register sheet application classes
    Actors.unregisterSheet("core", ActorSheet);
    Actors.registerSheet("m-d100", mActorSheet, {types: ["character", "creature"], makeDefault: true});
    Items.unregisterSheet("core", ItemSheet);
    Items.registerSheet("m-d100", mItemSheet, {makeDefault: true});
  
    // Register system settings
    game.settings.register("m-d100", "fixedActionPoints", {
      name: "Fixed Action Points",
      hint: "Set Action Points to 2 for ALL instead of standard calculation method. Requires a refresh (Hit F5).",
      scope: "world",
      config: true,
      default: false,
      type: Boolean
    });
  });