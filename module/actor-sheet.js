/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class mActorSheet extends ActorSheet {

    /** @override */
      static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
          classes: ["m-d100", "sheet", "actor"],
            width: 700,
            height: 800,
            tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}],
            dragDrop: [{dragSelector: [
                        ".item-list .item",
                        ".skill-list1 .item",
                        ".skill-list2 .item",
                        ".combatTabLists .item",
                        ".currencyList .item",
                        ".inventoryTable .item",
                        ".spell-list .item"
                      ], 
                      dropSelector: null}]
        });
        }
  
    /* -------------------------------------------- */

    /** @override */
    get template() {
        const path = "systems/m-d100/templates";
        return `${path}/${this.actor.data.type}-sheet.html`;
      }

  /* -------------------------------------------- */

    /** @override */
    getData() {
      const  data = super.getData(); 
      data.dtypes = ["String", "Number", "Boolean"];
      data.isGM = game.user.isGM;
      data.editable = data.options.editable;
      const actorData = data.data;
      data.actor = actorData;
      data.data = actorData.data;
      let options = 0;
      let user = this.user;
  
      // Prepare Items
      if (this.actor.data.type === 'character'||this.actor.data.type === 'npc'||this.actor.data.type === 'creature') {
        this._prepareCharacterItems(data);
      }
  
      return data;
      }
  
      _prepareCharacterItems(sheetData) {
        const actorData = sheetData.actor;
  
        //Initialize containers
        const equipment = [];
        const armor = [];
        const meleeWeapon = [];
        const rangedWeapon = [];
        const professionalSkill = [];
        const standardSkill = [];
        const magicSkill = [];
        const passion = [];
        const combatStyle = [];
        const hitLocation = [];
        const ammunition = [];
        const currency = [];
        const spell = {
          folkMagic: [],
          mysticism: [],
          sorcery: [],
          theism: [],
          animism: []
        };
  
        //Iterate through items, allocating to containers
        for (let i of sheetData.items) {
          let item = i.data;
          i.img = i.img || DEFAULT_TOKEN;
          //Append to item
          if (i.type === 'equipment') {
            equipment.push(i);
          }
          //Append to armor
          else if (i.type === 'armor') {
            armor.push(i);
          }
          //Append to meleeWeapon
          else if (i.type === "meleeWeapon") {
            meleeWeapon.push(i);
          }
          //Append to meleeWeapon
          else if (i.type === "rangedWeapon") {
            rangedWeapon.push(i);
          }
          //Append to profSkill
          else if (i.type === 'professionalSkill') {
            professionalSkill.push(i);
          }
          //Append to standardSkill
          else if (i.type === 'standardSkill') {
              standardSkill.push(i);
          }
          //Append to magicSkill
          else if (i.type === 'magicSkill') {
            magicSkill.push(i);
          }
          //Append to passion
          else if (i.type === 'passion') {
            passion.push(i);
          }
          //Append to combatStyle
          else if (i.type === 'combatStyle') {
            combatStyle.push(i);
          }
          //Append to hitLoc
          else if (i.type === 'hitLocation') {
              hitLocation.push(i);
          }
          //Append to ammunition
          else if (i.type === 'ammunition') {
              ammunition.push(i);
          }
          //Append to currency
          else if (i.type === 'currency') {
            currency.push(i);
          }
          //Append to spell
          else if (i.type === 'spell') {
            if (i.data.discipline !== undefined && i.data.discipline !== '') {
              spell[i.data.discipline].push(i);
            }
          }
        }
  
        //Assign and return
        actorData.equipment = equipment;
        actorData.armor = armor;
        actorData.meleeWeapon = meleeWeapon;
        actorData.rangedWeapon = rangedWeapon;
        actorData.professionalSkill = professionalSkill;
        actorData.standardSkill = standardSkill;
        actorData.magicSkill = magicSkill;
        actorData.passion = passion;
        actorData.combatStyle = combatStyle;
        actorData.hitLocation = hitLocation;
        actorData.ammunition = ammunition;
        actorData.currency = currency;
        actorData.spell = spell;
  
      }
  
    /* -------------------------------------------- */
  
    /** @override */
      activateListeners(html) {
      super.activateListeners(html);
  
      // Action & Roll Buttons
      html.find(".skill-roll").click(this._onSkillRoll.bind(this));
      html.find(".sendToChat").click(this._sendDescToChat.bind(this));
      html.find(".headerItem1").click(this._onHitLocationRoll.bind(this));
      html.find(".weapon-roll").click(this._onWeaponRoll.bind(this));
      html.find(".hpInputs").click(this._adjustCurrentHP.bind(this));
      html.find(".headerItem4").click(this._applyHealingRate.bind(this));
      html.find(".heightWeightChart").click(this._showHeightWeightChart.bind(this));
      html.find(".currencyQty").click(this._adjustCurrencyQty.bind(this));

      //For NPC's and Monsters
      html.find(".selectHitLocationArray").click(this._onSelectHitLocationArray.bind(this));

      //Add Item Buttons
      html.find(".itemCreate").click(this._onItemCreate.bind(this));
      html.find(".paperDoll img").click(this._onSwitchPaperDoll.bind(this));
  
      // Everything below here is only needed if the sheet is editable
      if (!this.options.editable) return;

      // Open Hit Location Armor Item
      html.find(".armorLabel").click( async (ev) => {
        const li = ev.currentTarget.id;
        const item = this.actor.items.get(li);
        item.sheet.render(true);
      })
  
      // Update Inventory Item
      html.find('.item-name').click( async (ev) => {
        const li = ev.currentTarget.closest(".item");
        const item = this.actor.items.get(li.dataset.itemId);
        item.sheet.render(true);
      });
  
      // Delete Inventory Item
      html.find('.item-delete').click(ev => {
        const li = $(ev.currentTarget).parents(".item");
        this.actor.deleteEmbeddedDocuments("Item", [li.data("itemId")]);
        li.slideUp(200, () => this.render(false));
      });
  
    }
  
    /**
     * Handle clickable rolls.
     * @param {Event} event   The originating click event
     * @private
     */

  async _onItemCreate(event) {
    event.preventDefault()
    const element = event.currentTarget
    let itemData;

    //Create itemData based on Item Type
    if (element.id === "combatStyle"){
      itemData = {
        name: element.id,
        type: element.id,
        data: {
          char1: "str",
          char2: "dex",
          encEffect: true
        }
      }
    } else if (element.id === "passion") {
      itemData = {
        name: element.id,
        type: element.id,
        data: {
          char1: "pow",
          char2: "cha"
        }
      }
    } else if (element.id === "standardSkill"||element.id === "professionalSkill"||element.id === "magicSkill") {
      itemData = {
        name: element.id,
        type: element.id,
        data: {
          char1: "str",
          char2: "str"
        }
      }
    } else if (element.id === "spell") {
      itemData = {
        name: element.id,
        type: element.id,
        data: {
          discipline: "folkMagic"
        }
      }
    } else if (element.id === "meleeWeapon") {
      itemData = {
        name: element.id,
        type: element.id,
        data: {
          addDamageMod: true
        }
      }
    } else {
      itemData = {
        name: element.id,
        type: element.id
      }
    }

    let newItem = this.actor.createEmbeddedDocuments("Item", [itemData]);
    console.log(newItem);

    if (await newItem.type === "combatStyle") {
      newItem.update({"data.char1": "str", "data.char2": "dex"})
    }
}

  async _onSwitchPaperDoll(event) {
    event.preventDefault()
    const element = event.currentTarget;
    let paperDoll = this.actor.data.data.paperDoll;

      let d = new Dialog({
        title: "Select Figure",
        content: `<form>
                    <div style="padding: 5px;">

                        <div>
                          <h2 style="text-align: center; font-size: small;">Select Figure for Character</h2>
                        </div>
                        <hr>

                      <div style="display: inline-block; width: 100%;">
                        <div style="float: left; border-right: groove;">
                          <label>
                            Figure 1
                            <p></p>
                            <img id="femFigure" src="systems/m-d100/images/paperDoll_Female.png" height="250" width="75" style="border: none;"/>
                            <input type="checkbox" id="femOption"/>
                          </label>
                        </div>

                        <div style="float: right; border-left: groove;">
                          <label>
                            Figure 2
                            <p></p>
                            <input type="checkbox" id="femHeroicOption"/>
                            <img id="femFigureHeroic" src="systems/m-d100/images/paperDoll_HeroicFemale.png" height="250" width="75" style="border: none;"/>
                          </label>
                        </div>
                      </div>

                      <hr>

                      <div style="display: inline-block; width: 100%;">
                      <div style="float: left; border-right: groove;">
                          <label>
                            Figure 3
                            <p></p>
                            <img id="manFigure" src="systems/m-d100/images/paperDoll_Male.png" height="250" width="75" style="border: none;"/>
                            <input type="checkbox" id="manOption"/>
                          </label>
                        </div>

                        <div style="float: right; border-left: groove;">
                          <label>
                            Figure 4
                            <p></p>
                            <input type="checkbox" id="manHeroicOption"/>
                            <img id="manFigureHeroic" src="systems/m-d100/images/paperDoll_HeroicMale.png" height="250" width="75" style="border: none;"/>
                          </label>
                        </div>

                      </div>

                    </div>
                  </form>`,
        buttons: {
          one: {
            label: "Submit",
            callback: html => {
              const maleDoll = html.find(`[id="manOption"]`)[0].checked;
              const maleHeroicDoll = html.find(`[id="manHeroicOption"]`)[0].checked;
              const femDoll = html.find(`[id="femOption"]`)[0].checked;
              const femHeroicDoll = html.find(`[id="femHeroicOption"]`)[0].checked;

              if (maleDoll){
                this.actor.data.data.paperDoll = "systems/m-d100/images/paperDoll_Male.png";
              } else if (femDoll){
                this.actor.data.data.paperDoll = "systems/m-d100/images/paperDoll_Female.png";
              } else if (maleHeroicDoll){
                this.actor.data.data.paperDoll = "systems/m-d100/images/paperDoll_HeroicMale.png";
              } else if (femHeroicDoll){
                this.actor.data.data.paperDoll = "systems/m-d100/images/paperDoll_HeroicFemale.png";
              }

              this.actor.update({"data.paperDoll": this.actor.data.data.paperDoll});
            }
          },
          two: {
            label: "Cancel",
            callback: html => console.log("Cancelled")
          }
        },
        default: "one",
        close: console.log()
      });
      d.render(true);

    console.log(paperDoll)
    await this.actor.update({"data.paperDoll": this.actor.data.data.paperDoll});
  }

  _onSkillRoll(event) {
    event.preventDefault()
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    const roll = new Roll("1d100");
    roll.roll();

    let label = "";
    let diffMods = [2, 1.5, 1, 2/3, 0.5, 0.1];
    let tableRows = [];
    let target = 0;

    //Create table entries for chat roll
    for (let i of diffMods) {
      if (i === 2){
        label = "Very Easy";
        target = Math.ceil(item.data.data.value * i);
      } else if (i === 1.5){
        label = "Easy";
        target = Math.ceil(item.data.data.value * i);
      } else if (i === 1){
        label = "Standard";
        target = Math.ceil(item.data.data.value * i);
      } else if (i === 2/3){
        label = "Hard";
        target = Math.ceil(item.data.data.value * i);
      } else if (i === 1/2){
        label = "Formidable";
        target = Math.ceil(item.data.data.value * i);
      } else if (i === 1/10){
        label = "Herculean";
        target = Math.ceil(item.data.data.value * i);
      }

      let rollResult = "";

      if (roll.result <= (Math.ceil(target * 0.1))) {
        rollResult = `<span style="color: goldenrod; font-weight: bold;">CRITICAL</span>`
      }
      else if (roll.result >= 99) {
        rollResult = `<span style="color: darkred; font-weight: bold;">FUMBLE</span>`
      }
      else if (roll.result <= target) {
        rollResult = `<span style="color: green; font-weight: bold;">SUCCESS</span>`
      }
      else if (roll.result > target) {
        rollResult = `<span style="color: red; font-weight: bold;">FAILURE</span>`
      }

      let row = `<tr style="font-weight: bold;">
                  <td>${label}</td>
                  <td>[[${roll.result}]]</td>
                  <td>[[${target}]]</td>
                  <td>${rollResult}</td>
                </tr>`
      
      tableRows.push(row);
    }

    //Create Roll Tags for to display difficulty modifiers on chat roll
    let fatigueDiff = "";
    const fatigue = this.actor.data.data.fatigue.current;

    //ENC Difficulty Tag
    let encTag = "";
    const currentENC = this.actor.data.data.encStats.encTotal;
    const actorSTR = this.actor.data.data.characteristics.str.value;
    for (let skill of this.actor.items.filter(i => i.data.data.hasOwnProperty("encEffect"))) {
      if (skill.data.data.encEffect && skill.data._id === item.data._id && currentENC > (actorSTR * 2) && currentENC <= (actorSTR * 3)) {
        encTag = `<div>
                      ENC Difficulty Modifier: <span style="border-radius: 25px; padding: 5px; background-color: orange; color: white;">Hard</span>
                    </div>`;

      } else if (skill.data.data.encEffect && skill.data._id === item.data._id && currentENC > (actorSTR * 3)) {
        encTag = `<div>
                      ENC Difficulty Modifier: <span style="border-radius: 25px; padding: 5px; background-color: red; color: white;">Formidable</span>
                    </div>`;

      }
    }

    //Fatigue Difficulty Tag
    if (fatigue === "fresh") {
      fatigueDiff = `<span>Standard</span>`;
    } else if (fatigue === "winded"||fatigue === "tired") {
      fatigueDiff = `<span style="border-radius: 25px; padding: 5px; background-color: orange; color: white;">Hard</span>`;
    } else if (fatigue === "wearied"||fatigue === "exhausted") {
      fatigueDiff = `<span style="border-radius: 25px; padding: 5px; background-color: red; color: white;">Formidable</span>`;
    } else if (fatigue === "debilitated"||fatigue === "incapacitated") {
      fatigueDiff = `<span style="border-radius: 25px; padding: 5px; background-color: darkred; color: white;">Herculean</span>`;
    } else {
      fatigueDiff = `<span style="border-radius: 25px; padding: 5px; background-color: black; color: white;">Hopeless</span>`
    }

    let contentString = `<h2 style="font-size: 16px; font-weight: bold;">${item.name}</h2>
                        <table>
                          <tr>
                            <th>Difficulty</th>
                            <th>Roll</th>
                            <th>Target</th>
                            <th>Result</th>
                          </tr>
                          ${tableRows.join("")}
                        </table>`

    ChatMessage.create({
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      content: contentString,
      flavor: `Fatigue Difficulty Modifier: ${fatigueDiff} ${encTag}`,
      roll: roll
    })


  }

  _sendDescToChat(event) {
    event.preventDefault()
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    ChatMessage.create({
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      flavor: `Spell Type: ${item.data.data.discipline}`,
      content: `<h2>${item.name}</h2>
                <div>
                  ${item.data.data.desc}
                </div>`
    });
  }

  _onHitLocationRoll(event){
    event.preventDefault()
    const element = event.currentTarget;

    const roll = new Roll("1d20");
    roll.roll();

    const hitLocations = this.actor.items.filter(i => i.type === "hitLocation");
    let hitLocationLabel = "";

    for (let i of hitLocations) {
      let rangeStart = i.data.data.rangeStart;
      let rangeEnd = i.data.data.rangeEnd;

      for (let x = rangeStart; x <= rangeEnd; x++) {
        if (roll.result == x) {
          hitLocationLabel = i.name;
        }
      } 
    }

    let contentString = `<h2 style="font-size: 16px; font-weight: bold;">${hitLocationLabel}</h2>
                          <div>[[${roll.result}]]</div>`;

    ChatMessage.create({
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      flavor: `${this.actor.name} Hit Location Result`,
      content: contentString,
      roll: roll
    })
  }

  _onWeaponRoll(event) {
    event.preventDefault()
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    // Collect & prepare data from the weapon for roll
    let damageMod = "";
    item.data.data.addDamageMod ? damageMod = this.actor.data.data.attributes.damageMod.value : damageMod = 0;

    console.log(item);

    const weaponDamage = `${item.data.data.dmg} + ${damageMod}`;
    const roll = new Roll(weaponDamage);
    console.log(weaponDamage);
    roll.roll();

    let contentString = "";


    // Create Ouputs for Ranged/Melee Weapons
    if (item.type === "meleeWeapon") {
      contentString = `<h2 style="font-size: 16px; font-weight: bold;">${item.name}</h2>
                        <div style="display: flex; justify-content: space-between;">
                          <label><b>Damage: [[${roll.result}]]</b></label>
                          <label><b>Reach:</b> ${item.data.data.reach}</label>
                          <label><b>Size:</b> ${item.data.data.size}</label>
                        </div>
                        <hr>`
    } else if (item.type === "rangedWeapon") {
      contentString = `<h2 style="font-size: 16px; font-weight: bold;">${item.name}</h2>
                      <div style="display: flex; justify-content: space-between;">
                        <label><b>Damage: [[${roll.result}]]</b></label>
                        <label><b>Load:</b> ${item.data.data.load}</label>
                        <label><b>Force:</b> ${item.data.data.force}</label>
                      </div>
                      <hr>`
    }

    // Create Chat Message
    ChatMessage.create({
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      flavor: `${this.actor.name} attacks with their ${item.name}`,
      content: contentString,
      roll: roll
    })


  }

  _adjustCurrentHP(event) {
    event.preventDefault()
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    let d = new Dialog({
      title: "Adjust Current HP",
      content: `<form>
                  <div style="text-align: center;">
                    <i>Increase or Decrease HP for ${item.name}</i>
                  </div>
                  <hr>
                  <div>
                    <input type="number" id="playerInput" placeholder="input negative or positive value">
                  </div>
                  <hr>
                </form>`,
      buttons: {
        one: {
          label: "Submit",
          callback: html => {
            const playerInput = parseInt(html.find(`[id="playerInput"]`).val());

            item.update({"data.hpValue": item.data.data.hpValue + playerInput});
          }
        },
        two: {
          label: "Cancel",
          callback: html => console.log("Cancelled")
        }
      },
      default: "one",
      close: html => console.log()
    });

    d.render(true);
  }

  async _applyHealingRate(event) {
    event.preventDefault()

    const hitLocations = this.actor.items.filter(i => i.type === "hitLocation");
    const healRate = this.actor.data.data.attributes.healRate.value;

    for (let bodyPart of hitLocations) {
      if (bodyPart.data.data.hpValue <= bodyPart.data.data.hpMax - healRate) {
        await bodyPart.update({"data.hpValue": bodyPart.data.data.hpValue + healRate});
      } else if (bodyPart.data.data.hpValue < bodyPart.data.data.hpMax) {
          await bodyPart.update({"data.hpValue": bodyPart.data.data.hpMax});
      }
    }

    let sound = new Audio("/systems/m-d100/sounds/bandage.ogg");
    sound.play();
  }

  _showHeightWeightChart(event) {
    event.preventDefault()
    const tableEntryArray =[];
    let heightStart = 151;
    let weightStart = 36;
    let weightEnd = 72;

    for (let i=8; i <= 20; i++) {
      let entry = `<tr>
                    <td>${i}</td>
                    <td>${heightStart} - ${heightStart + 4}</td>
                    <td>${weightStart} - ${weightEnd}</td>
                  </tr>`;
      tableEntryArray.push(entry);

      heightStart = heightStart + 5;
      weightStart = weightStart + 5;
      weightEnd = weightEnd + 9;
    }

    let d = new Dialog({
      title: "Height & Weight Chart",
      content: `<form>
                  <div>
                    <h2>Height & Weight Chart</h2>
                  </div>

                    <div>
                      <div>
                        <img src="/systems/m-d100/images/heightChartFinal.png"/>
                      </div>
                    </div>

                    <div>
                      <table style="text-align: center;">
                        <tr>
                          <th>SIZ</th>
                          <th>Height(cm)</th>
                          <th>Weight(kg)</th>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>1 - 45</td>
                          <td>1 - 9</td>
                        </tr>
                        <tr>
                          <td>2</td>
                          <td>46 - 80</td>
                          <td>6 - 18</td>
                        </tr>
                        <tr>
                          <td>3</td>
                          <td>81 - 105</td>
                          <td>11 - 27</td>
                        </tr>
                        <tr>
                          <td>4</td>
                          <td>106 - 120</td>
                          <td>16 - 36</td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>121 - 130</td>
                          <td>21 - 45</td>
                        </tr>
                        <tr>
                          <td>6</td>
                          <td>131 - 140</td>
                          <td>26 - 54</td>
                        </tr>
                        <tr>
                          <td>7</td>
                          <td>141 - 150</td>
                          <td>31 - 63</td>
                        </tr>
                        ${tableEntryArray.join("")}

                      </table>
                    </div>

                </form>`,
      buttons: {
        one: {
          label: "Close",
          callback: html => console.log("Cancelled")
        }
      },
      default: "one",
      close: html => console.log()
    });

    d.render(true);
  }

  _adjustCurrencyQty(event) {
    event.preventDefault()
    const element = event.currentTarget;
    const li = element.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    const currencyQty = item.data.data.qty;

    let d = new Dialog({
      title: "Adjust Currency Qty",
      content: `<form>
                  <div>
                  <hr>
                    <div style="text-align: center;">
                      Increase/Decrease currency qty by a specified amount
                    </div>

                    <div>
                      <input type="number" id="playerInput" placeholder="Number to add or subtract from current qty"/>
                    </div>
                    <hr>
                  </div>
                </form>`,
      buttons: {
        one: {
          label: "Submit",
          callback: html => {
            const playerInput = parseInt(html.find(`[id="playerInput"]`).val());
            item.update({"data.qty": currencyQty + playerInput});

            let sound = new Audio("/systems/m-d100/sounds/coinJingle.ogg");
            sound.play();
          }
        },
        two: {
          label: "Cancel",
          callback: html => console.log("Cancelled")
        }
      },
      default: "one",
      close: html => console.log()
    });

    d.render(true);

    console.log(item);
  }

  _onSelectHitLocationArray(event) {
    event.preventDefault()


    let d = new Dialog({
      title: "Hit Location Templates",
      content: `<form>

                  <div style="text-align: center;">
                    <label>
                      Select a Hit Location Template to add to this character.
                      <div>
                        <i>Selecting more than one will add ALL selected templates.</i>
                      </div>
                    </label>
                  </div>

                  <hr>

                  <div style="display: flex; justify-content: space-between; flex-wrap: wrap;">
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="humanoid"/>
                        <label>Humanoid</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="arachnid"/>
                        <label>Arachnid</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="centaurid"/>
                        <label>Centaurid</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="draconic"/>
                        <label>Draconic</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="insect"/>
                        <label>Insect</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="dorsalFinAquatic"/>
                        <label>Dorsal Fin Aquatic</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="pachyderm"/>
                        <label>Pachyderm</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="serpentine"/>
                        <label>Serpentine</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="tailedArachnid"/>
                        <label>Tailed Arachnid</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="tailedBiped"/>
                        <label>Tailed Biped</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="tailedQuadruped"/>
                        <label>Tailed Quadruped</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="wingedBiped"/>
                        <label>Winged Biped</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="wingedInsect"/>
                        <label>Winged Insect</label>
                    </div>
                    <div style="flex: 0 0 50%">
                        <input type="checkbox" id="wingedQuadruped"/>
                        <label>Winged Quadruped</label>
                    </div>
                  </div>

                  <hr>

                </form>`,
      buttons: {
        one: {
          label: "Add Hit Locations",
          callback: async (html) => {
            const humanoid = html.find(`[id="humanoid"]`)[0].checked;
            const arachnid = html.find(`[id="arachnid"]`)[0].checked;
            const centaurid = html.find(`[id="centaurid"]`)[0].checked;
            const draconic = html.find(`[id="draconic"]`)[0].checked;
            const insect = html.find(`[id="insect"]`)[0].checked;
            const dorsalFinAquatic = html.find(`[id="dorsalFinAquatic"]`)[0].checked;
            const pachyderm = html.find(`[id="pachyderm"]`)[0].checked;
            const serpentine = html.find(`[id="serpentine"]`)[0].checked;
            const tailedArachnid = html.find(`[id="tailedArachnid"]`)[0].checked;
            const tailedBiped = html.find(`[id="tailedBiped"]`)[0].checked;
            const tailedQuadruped = html.find(`[id="tailedQuadruped"]`)[0].checked;
            const wingedBiped = html.find(`[id="wingedBiped"]`)[0].checked;
            const wingedInsect = html.find(`[id="wingedInsect"]`)[0].checked;
            const wingedQuadruped = html.find(`[id="wingedQuadruped"]`)[0].checked;

            const nameLabelArray = ["arachnid",
                                    "centaurid",
                                    "draconic",
                                    "insect",
                                    "dorsalFinAquatic",
                                    "pachyderm",
                                    "serpentine",
                                    "tailedArachnid",
                                    "tailedBiped",
                                    "tailedQuadruped",
                                    "wingedBiped",
                                    "wingedInsect",
                                    "wingedQuadruped"
                                  ];

            //Grab and add Hit Locations based on selection from dialog
            //Humanoid has it's own because I didn't name the Hit Location Compendium appropriately
            if (humanoid) {
              let hitLocationsPack = game.packs.get("m-d100.human-hit-locations");
              let collection = await hitLocationsPack.getDocuments();
              
              collection.sort(function (a, b) {
                let rangeStart1 = a.data.data.rangeStart;
                let rangeStart2 = b.data.data.rangeStart;
                if (rangeStart1 < rangeStart2) {
                  return -1;
                } if (rangeStart1 > rangeStart2) {
                  return 1;
                }
                return 0
              });
              this.actor.update({
                items: collection.map(i => i.toObject())
              });
            }

            //Grab other hit locations aside from Humanoid
            for (let template of nameLabelArray) {
              if (eval(template) === true) {

                let packPath = `m-d100.${template}-hit-locations`

                let hitLocationsPack = game.packs.get(packPath);
                let collection = await hitLocationsPack.getDocuments();
                
                collection.sort(function (a, b) {
                  let rangeStart1 = a.data.data.rangeStart;
                  let rangeStart2 = b.data.data.rangeStart;
                  if (rangeStart1 < rangeStart2) {
                    return -1;
                  } if (rangeStart1 > rangeStart2) {
                    return 1;
                  }
                  return 0
                });
                this.actor.update({
                  items: collection.map(i => i.toObject())
                });
              }
            }
          }
        },
        two: {
          label: "Cancel",
          callback: html => console.log("Cancelled")
        }
      },
      default: "one",
      close: html => console.log()
    });

    d.render(true);
  }

}